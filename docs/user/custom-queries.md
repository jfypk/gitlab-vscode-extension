---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Custom queries

This page has moved [into the GitLab documentation](https://docs.gitlab.com/ee/editor_extensions/visual_studio_code/custom_queries.html).
