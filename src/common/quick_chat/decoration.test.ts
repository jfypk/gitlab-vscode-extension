import vscode, { CommentThreadCollapsibleState } from 'vscode';
import { createFakePartial } from '../test_utils/create_fake_partial';
import * as constants from '../constants';
import { QuickChatDecoration } from './decoration';
import { COMMENT_CONTROLLER_ID } from './utils';

describe('QuickChatDecoration', () => {
  let mockDecoration: QuickChatDecoration;
  let mockExtensionContext: vscode.ExtensionContext;
  let mockEditor: vscode.TextEditor;
  let mockFileUri: vscode.Uri;

  beforeEach(() => {
    mockExtensionContext = createFakePartial<vscode.ExtensionContext>({
      extensionUri: vscode.Uri.file('/foo/bar'),
    });

    mockFileUri = vscode.Uri.file('test/file.ts');
    mockEditor = createFakePartial<vscode.TextEditor>({
      selection: createFakePartial<vscode.Selection>({ active: new vscode.Position(0, 0) }),
      document: createFakePartial<vscode.TextDocument>({
        lineAt: jest.fn().mockReturnValue({ isEmptyOrWhitespace: true }),
        uri: mockFileUri, // Default to a file URI
      }),
      setDecorations: jest.fn(),
    });
    vscode.window.activeTextEditor = mockEditor;
    vscode.workspace.getConfiguration = jest
      .fn()
      .mockReturnValue({ keybindingHints: { enabled: true } });
    mockDecoration = new QuickChatDecoration(mockExtensionContext);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('updateKeybindingHint', () => {
    let mockKeybindingHintDecoration: jest.Mock;

    beforeEach(() => {
      mockKeybindingHintDecoration = jest.fn().mockReturnValue({});
      (vscode.window.createTextEditorDecorationType as jest.Mock).mockImplementation(
        mockKeybindingHintDecoration,
      );
    });

    afterEach(() => jest.restoreAllMocks());

    it.each`
      isOSX    | expectedContentText         | description
      ${false} | ${'(Alt+C) Duo Quick Chat'} | ${'non-macOS'}
      ${true}  | ${'⌥C Duo Quick Chat'}      | ${'macOS'}
    `('should create correct decoration for $description', ({ isOSX, expectedContentText }) => {
      jest.mocked(constants).IS_OSX = isOSX;

      mockDecoration.updateKeybindingHint(mockEditor);

      expect(mockKeybindingHintDecoration).toHaveBeenCalledWith({
        after: {
          contentText: expectedContentText,
          margin: '0 0 0 2ch',
          color: new vscode.ThemeColor('editorHint.foreground'),
          fontStyle: 'normal',
          textDecoration: 'none; filter: opacity(0.34);',
        },
      });
    });
  });

  describe('updateGutterIcon', () => {
    let mockGutterIconDecoration: vscode.TextEditorDecorationType;

    beforeEach(() => {
      mockGutterIconDecoration = createFakePartial<vscode.TextEditorDecorationType>({
        dispose: jest.fn(),
      });
      (vscode.window.createTextEditorDecorationType as jest.Mock).mockReturnValue(
        mockGutterIconDecoration,
      );
    });

    it('sets gutter icon decoration at the specified position', () => {
      const position = new vscode.Position(1, 1);

      mockDecoration.updateGutterIcon(mockEditor, position);

      expect(vscode.window.createTextEditorDecorationType).toHaveBeenCalledWith({
        gutterIconPath: vscode.Uri.joinPath(
          vscode.Uri.file('/foo/bar'),
          'assets/icons/gitlab-duo-quick-chat.svg',
        ),
      });
    });
  });

  describe('toggleGutterIcon', () => {
    let mockGutterIconDecoration: vscode.TextEditorDecorationType;

    beforeEach(() => {
      mockGutterIconDecoration = createFakePartial<vscode.TextEditorDecorationType>({
        dispose: jest.fn(),
      });
      (vscode.window.createTextEditorDecorationType as jest.Mock).mockReturnValue(
        mockGutterIconDecoration,
      );
      mockDecoration.resetGutterIcon = jest.fn();
      mockDecoration.updateGutterIcon = jest.fn();
    });

    describe('hides gutter icon', () => {
      it('when thread is collapsed', () => {
        const mockThread = createFakePartial<vscode.CommentThread>({
          comments: [],
          range: new vscode.Range(0, 0, 0, 0),
          uri: mockFileUri,
          collapsibleState: CommentThreadCollapsibleState.Collapsed,
        });
        mockDecoration.toggleGutterIcon(mockThread);

        // icon is disposed
        expect(mockDecoration.resetGutterIcon).toHaveBeenCalled();
        // updateGutterIcon is not called to recreate an icon decoration
        expect(mockDecoration.updateGutterIcon).not.toHaveBeenCalled();
      });

      it('for different documents', () => {
        const mockThread = createFakePartial<vscode.CommentThread>({
          comments: [],
          range: new vscode.Range(0, 0, 0, 0),
          uri: vscode.Uri.file('test/notActiveFile.ts'),
          collapsibleState: CommentThreadCollapsibleState.Expanded,
        });
        mockDecoration.toggleGutterIcon(mockThread);

        // icon is disposed
        expect(mockDecoration.resetGutterIcon).toHaveBeenCalled();
        // updateGutterIcon is not called to recreate an icon decoration
        expect(mockDecoration.updateGutterIcon).not.toHaveBeenCalled();
      });
    });

    describe('shows gutter icon ', () => {
      it('when in comment input', async () => {
        const mockThread = createFakePartial<vscode.CommentThread>({
          comments: [],
          range: new vscode.Range(0, 0, 0, 0),
          uri: mockFileUri,
          collapsibleState: CommentThreadCollapsibleState.Expanded,
        });
        // Create new mock editor with comment input URI
        const commentInputEditor = createFakePartial<vscode.TextEditor>({
          document: {
            uri: createFakePartial<vscode.Uri>({
              authority: COMMENT_CONTROLLER_ID,
              scheme: 'comment',
            }),
          },
          selection: new vscode.Selection(0, 0, 0, 0),
          setDecorations: jest.fn(),
        });

        (vscode.window.visibleTextEditors as unknown) = [mockEditor];
        (vscode.window.activeTextEditor as unknown) = commentInputEditor;

        mockDecoration.toggleGutterIcon(mockThread);

        // icon is disposed
        expect(mockDecoration.resetGutterIcon).toHaveBeenCalled();

        // updateGutterIcon is called with active editor NOT comment input editor
        expect(mockDecoration.updateGutterIcon).toHaveBeenCalledWith(
          mockEditor,
          mockThread.range.end,
        );
      });

      it('when in active editor', () => {
        const mockThread = createFakePartial<vscode.CommentThread>({
          comments: [],
          range: new vscode.Range(0, 0, 0, 0),
          uri: mockFileUri,
          collapsibleState: CommentThreadCollapsibleState.Expanded,
        });
        mockDecoration.toggleGutterIcon(mockThread);
        // icon is disposed
        expect(mockDecoration.resetGutterIcon).toHaveBeenCalled();
        // updateGutterIcon is called to create an icon decoration
        expect(mockDecoration.updateGutterIcon).toHaveBeenCalledWith(
          mockEditor,
          mockThread.range.end,
        );
      });
    });
  });
});
