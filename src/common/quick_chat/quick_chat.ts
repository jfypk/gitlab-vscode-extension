import vscode from 'vscode';
import { v4 as uuidv4 } from 'uuid';
import { debounce } from 'lodash';
import { log } from '../log';
import { AiCompletionResponseMessageType } from '../api/graphql/ai_completion_response_channel';
import { CONFIG_NAMESPACE } from '../constants';
import { GitLabPlatformManagerForChat } from '../chat/get_platform_manager_for_chat';
import { AiActionResponseType, GitLabChatApi } from '../chat/gitlab_chat_api';
import {
  getActiveFileContext,
  getFileContext,
  GitLabChatFileContext,
} from '../chat/gitlab_chat_file_context';
import { AIContextManager } from '../chat/ai_context_manager';
import { SPECIAL_MESSAGES } from '../chat/constants';
import { doNotAwait } from '../utils/do_not_await';
import {
  FeatureFlag,
  getLocalFeatureFlagService,
} from '../feature_flags/local_feature_flag_service';
import {
  addCopyAndInsertButtonsToCodeBlocks,
  calculateThreadRange,
  COMMENT_CONTROLLER_ID,
  createChatResetComment,
  createComment,
  createCommentController,
  createGutterIconDecoration,
  createKeybindingHintDecoration,
  createLoaderComment,
  generateThreadLabel,
  getLastCommentContextValue,
  MarkdownProcessorPipeline,
  provideCompletionItems,
  setLoadingContext,
} from './utils';
import {
  COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT,
  COMMAND_INSERT_CODE_SNIPPET_FROM_QUICK_CHAT,
  COMMAND_QUICK_CHAT_OPEN_TELEMETRY,
  COMMAND_QUICK_CHAT_MESSAGE_TELEMETRY,
  QUICK_CHAT_OPEN_TRIGGER,
  COMMAND_OPEN_QUICK_CHAT,
  COMMAND_SEND_QUICK_CHAT,
  COMMAND_SEND_QUICK_CHAT_DUPLICATE,
  COMMAND_OPEN_QUICK_CHAT_WITH_SHORTCUT,
  COMMAND_CLOSE_QUICK_CHAT,
  COMMAND_SHOW_AND_SEND_QUICK_CHAT_WITH_CONTEXT,
} from './constants';

interface QuickChatMessageSentOptions {
  message: string;
}

export interface QuickChatOpenOptionsBase {
  trigger: QUICK_CHAT_OPEN_TRIGGER;
  message?: string;
}

export interface QuickChatOpenOptionsWithSelection extends QuickChatOpenOptionsBase {
  document: vscode.TextDocument;
  range: vscode.Range | vscode.Selection;
}

export interface QuickChatOpenOptionsWithoutSelection extends QuickChatOpenOptionsBase {
  document?: never;
  range?: never;
}

export type QuickChatOpenOptions =
  | QuickChatOpenOptionsWithSelection
  | QuickChatOpenOptionsWithoutSelection;

export class QuickChat {
  #api: GitLabChatApi;

  #commentController: vscode.CommentController;

  #extensionContext: vscode.ExtensionContext;

  #response: vscode.MarkdownString | null = null;

  #thread: vscode.CommentThread | null = null;

  #nextExpectedChunkId: number = 1;

  #chunkBuffer: { [id: number]: string | undefined } = {};

  #markdownPipeline = new MarkdownProcessorPipeline([addCopyAndInsertButtonsToCodeBlocks]);

  #disposables: vscode.Disposable[] = [];

  #keybindingHintDecoration = createKeybindingHintDecoration();

  #isKeybindingHintEnabled: vscode.WorkspaceConfiguration =
    vscode.workspace.getConfiguration(CONFIG_NAMESPACE)?.keybindingHints?.enabled;

  #gutterIconDecoration: vscode.TextEditorDecorationType | null = null;

  #isOpen: boolean = false;

  constructor(
    manager: GitLabPlatformManagerForChat,
    context: vscode.ExtensionContext,
    aiContextManager: AIContextManager,
  ) {
    this.#api = new GitLabChatApi(manager, [], aiContextManager);
    this.#commentController = createCommentController();
    this.#extensionContext = context;

    const handleSelectionChangeDebounced = debounce(
      (event: vscode.TextEditorSelectionChangeEvent) => {
        this.#updateThreadLabel(event);
        this.#updateKeybindingHint(event.textEditor);
      },
      100,
    );

    this.#disposables.push(
      this.#commentController,

      // Registers a completion item provider for quick actions (e.g. /tests)
      vscode.languages.registerCompletionItemProvider(
        { scheme: 'comment', pattern: '**' },
        { provideCompletionItems },
        '/',
      ),

      vscode.window.onDidChangeTextEditorSelection(handleSelectionChangeDebounced),

      vscode.workspace.onDidChangeConfiguration(() => {
        this.#isKeybindingHintEnabled =
          vscode.workspace.getConfiguration(CONFIG_NAMESPACE)?.keybindingHints?.enabled;
      }),

      vscode.window.onDidChangeTextEditorVisibleRanges(async event => {
        if (event.textEditor.document.uri === this.#thread?.uri) {
          this.#toggleGutterIcon();
          // listening for changes in visible text ranges to sync open/close context state via mouse clicks
          await this.#setVisibilityState(event.textEditor.document.uri);
        }
      }),

      vscode.window.onDidChangeActiveTextEditor(async event => {
        await this.#setVisibilityState(event?.document.uri);
      }),

      vscode.workspace.onDidChangeTextDocument(event => {
        if (!this.#thread || event.document.uri !== this.#thread.uri) return;

        const updatedRange = calculateThreadRange(this.#thread, event.contentChanges[0]);

        if (!updatedRange) return;

        this.#thread.range = updatedRange;
        this.#toggleGutterIcon();
      }),
      vscode.commands.registerCommand(COMMAND_OPEN_QUICK_CHAT, () => this.show()),
      vscode.commands.registerCommand(COMMAND_SEND_QUICK_CHAT, (reply: vscode.CommentReply) =>
        this.send(reply),
      ),
      vscode.commands.registerCommand(
        COMMAND_SEND_QUICK_CHAT_DUPLICATE,
        (reply: vscode.CommentReply) => this.send(reply),
      ),
      vscode.commands.registerCommand(COMMAND_OPEN_QUICK_CHAT_WITH_SHORTCUT, () =>
        this.show({ trigger: QUICK_CHAT_OPEN_TRIGGER.SHORTCUT }),
      ),
      vscode.commands.registerCommand(
        COMMAND_SHOW_AND_SEND_QUICK_CHAT_WITH_CONTEXT,
        (openOptions: QuickChatOpenOptions) => this.show(openOptions),
      ),
      vscode.commands.registerCommand(COMMAND_CLOSE_QUICK_CHAT, async () => {
        if (this.#thread?.collapsibleState === vscode.CommentThreadCollapsibleState.Expanded) {
          await this.hide();
        }
      }),
      this.#keybindingHintDecoration,
    );
  }

  async show(context?: QuickChatOpenOptions) {
    this.#trackChatOpenTelemetry(context);
    const editor = vscode.window.activeTextEditor;
    if (!editor) return;

    const prompt = 'Ask a question or give an instruction...';
    const title = 'GitLab Duo Quick Chat';

    const message =
      context?.message ?? (await vscode.window.showInputBox({ placeHolder: prompt, title }));
    if (!message?.trim()) return; // Exit if user cancels or input is empty

    this.#thread?.dispose();
    this.#api.clearChat().catch(log.error);

    const range = context?.range ?? new vscode.Range(editor.selection.start, editor.selection.end);
    this.#commentController.options = { prompt };

    // VS Code v1.98.0 and above requires commenting ranges to be provided to enable mouseDown event within the commentsController.
    // https://github.com/microsoft/vscode/commit/a9a797c322bcfdef9f0a9f929feb3b4008c7732b
    this.#commentController.commentingRangeProvider = {
      provideCommentingRanges: () => {
        return [new vscode.Range(range.end, range.end)];
      },
    };

    const document = context?.document ?? editor.document;

    this.#thread = this.#commentController.createCommentThread(document.uri, range, []);
    await setLoadingContext(false);
    this.#thread.collapsibleState = vscode.CommentThreadCollapsibleState.Expanded;
    this.#thread.label = generateThreadLabel(range);
    this.#toggleGutterIcon();

    await this.send({ text: message, thread: this.#thread }, context);
  }

  // used with custom keybinding to collapse quickChat
  async hide() {
    if (this.#thread?.collapsibleState === vscode.CommentThreadCollapsibleState.Expanded) {
      this.#thread.collapsibleState = vscode.CommentThreadCollapsibleState.Collapsed;
    }
  }

  async send(reply: vscode.CommentReply, openContext?: QuickChatOpenOptions) {
    this.#trackMessageSentTelemetry({ message: reply.text });
    const isHandled = await this.#tryHandleSpecialMessage(reply.text);
    if (isHandled) return;

    this.#nextExpectedChunkId = 1;
    this.#chunkBuffer = {};
    this.#thread = reply.thread;
    this.#thread.comments = [
      ...(this.#thread.comments || []),
      createComment(reply.text, 'user', 'You'),
      createLoaderComment(), // shows 'GitLab Duo Chat is finding an answer' loading message
    ];

    let fileContext: GitLabChatFileContext | undefined;
    if (openContext?.document && openContext?.range) {
      fileContext = getFileContext(openContext.document, openContext.range);
    } else {
      fileContext = getActiveFileContext();
    }

    const subscriptionId = uuidv4();
    await setLoadingContext(true); // disables the 'Send' button
    await this.#api.subscribeToUpdates(this.#subscriptionUpdateHandler.bind(this), subscriptionId);
    this.#submitQuestion(reply, subscriptionId, fileContext).catch(log.error);
  }

  async #submitQuestion(
    reply: vscode.CommentReply,
    subId: string,
    fileContext?: GitLabChatFileContext,
  ): Promise<AiActionResponseType> {
    return this.#api.processNewUserPrompt(reply.text, subId, fileContext);
  }

  async #subscriptionUpdateHandler(data: AiCompletionResponseMessageType) {
    this.#hideLoaderComment();
    this.#createResponseComment();
    this.#appendResponseComment(data);
    await setLoadingContext(false);
  }

  async #tryHandleSpecialMessage(text: string) {
    switch (text.trim().toLowerCase()) {
      case SPECIAL_MESSAGES.CLEAR:
        await this.#clearChat();
        return true;
      case SPECIAL_MESSAGES.RESET:
        await this.#resetChat();
        return true;
      default:
        // Not a special message
        return false;
    }
  }

  async #clearChat() {
    await setLoadingContext(false);
    if (this.#thread) {
      this.#thread.comments = [];
    }
    this.#api.clearChat().catch(log.error);
    // FIXME: ensure input is focused once we have added this capability: https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1595
  }

  async #resetChat() {
    await setLoadingContext(false);
    if (this.#thread?.comments) {
      this.#thread.comments = [...this.#thread.comments, createChatResetComment()];
    }
    return this.#api.resetChat();
    // FIXME: ensure input is focused once we have added this capability: https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1595
  }

  #hideLoaderComment() {
    if (!this.#thread || getLastCommentContextValue(this.#thread) !== 'loader') return;
    this.#thread.comments = this.#thread.comments.slice(0, -1);
  }

  #createResponseComment() {
    if (!this.#thread || getLastCommentContextValue(this.#thread) === 'response') return;
    this.#response = new vscode.MarkdownString('');
    this.#thread.comments = [
      ...this.#thread.comments,
      createComment(this.#response, 'response', 'Duo'),
    ];
  }

  #appendResponseComment(data: AiCompletionResponseMessageType) {
    if (!this.#thread || !this.#response) return;

    if (data.chunkId) {
      this.#processChunk(data.chunkId, data.content);
    } else {
      this.#processFullResponse(data.content);
    }

    this.#thread.comments = [...this.#thread.comments];
  }

  #processChunk(chunkId: number, content: string) {
    // Chunks may arrive out of order, so we buffer them and process in sequence
    this.#chunkBuffer[chunkId] = content;
    const chunkContent = this.#chunkBuffer[this.#nextExpectedChunkId];
    if (!this.#response || !chunkContent) return;

    this.#response.appendMarkdown(chunkContent);
    this.#chunkBuffer[this.#nextExpectedChunkId] = undefined;
    this.#nextExpectedChunkId++;
  }

  #processFullResponse(content: string) {
    if (!this.#response) return;

    this.#response.isTrusted = {
      enabledCommands: [
        COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT,
        COMMAND_INSERT_CODE_SNIPPET_FROM_QUICK_CHAT,
      ],
    };
    this.#response.value = this.#markdownPipeline.process(content);
  }

  #updateThreadLabel(event: vscode.TextEditorSelectionChangeEvent) {
    if (!this.#thread) return;

    const { scheme, authority } = event.textEditor.document.uri;

    const isInCommentInput = scheme === 'comment' && authority === COMMENT_CONTROLLER_ID;

    //  we do not want to recalculate the label because when the focus moves to the input
    // the editor selection becomes empty and it is reflected on the label
    if (isInCommentInput) return;

    const editor = event.textEditor;

    if (editor) {
      const { selection } = editor;

      const range = new vscode.Range(selection.start, selection.end);

      const newText = generateThreadLabel(range);

      if (this.#thread.label !== newText) {
        log.debug(
          `[QuickChat] Selection changed, updating quick chat label: ${newText}, ${this.#thread.label}`,
        );
        this.#thread.label = newText;
      }
    }
  }

  #updateKeybindingHint(editor: vscode.TextEditor) {
    editor.setDecorations(this.#keybindingHintDecoration, []);

    const position = editor.selection.active;
    const currentLine = editor.document.lineAt(position.line);
    const isFile = editor.document.uri.scheme === 'file';

    if (
      !this.#isKeybindingHintEnabled ||
      this.#isOpen ||
      !currentLine?.isEmptyOrWhitespace ||
      !isFile
    )
      return;

    editor.setDecorations(this.#keybindingHintDecoration, [currentLine.range]);
  }

  #toggleGutterIcon() {
    this.#gutterIconDecoration?.dispose();
    const editor = vscode.window.activeTextEditor;
    const threadUri = this.#thread?.uri;
    const isInCommentInput = editor?.document.uri.authority === COMMENT_CONTROLLER_ID;
    const isCollapsed =
      this.#thread?.collapsibleState === vscode.CommentThreadCollapsibleState.Collapsed;

    // If we're in comment input, find the editor for the thread's document
    const targetEditor = isInCommentInput
      ? vscode.window.visibleTextEditors.find(e => e.document.uri === threadUri)
      : editor;

    const shouldShowIcon = targetEditor && !isCollapsed && targetEditor.document.uri === threadUri;

    if (!shouldShowIcon || !this.#thread) return;

    this.#gutterIconDecoration = createGutterIconDecoration(this.#extensionContext.extensionUri);
    targetEditor.setDecorations(this.#gutterIconDecoration, [
      {
        range: new vscode.Range(this.#thread.range.end, this.#thread.range.end),
      },
    ]);
  }

  #trackChatOpenTelemetry(context?: QuickChatOpenOptions) {
    if (!getLocalFeatureFlagService().isEnabled(FeatureFlag.LanguageServer)) return;

    doNotAwait(
      vscode.commands.executeCommand(COMMAND_QUICK_CHAT_OPEN_TELEMETRY, {
        trigger:
          context?.trigger === QUICK_CHAT_OPEN_TRIGGER.SHORTCUT
            ? QUICK_CHAT_OPEN_TRIGGER.SHORTCUT
            : QUICK_CHAT_OPEN_TRIGGER.CLICK,
      }),
    );
  }

  #trackMessageSentTelemetry(context: QuickChatMessageSentOptions) {
    if (!getLocalFeatureFlagService().isEnabled(FeatureFlag.LanguageServer)) return;

    doNotAwait(
      vscode.commands.executeCommand(COMMAND_QUICK_CHAT_MESSAGE_TELEMETRY, {
        message: context.message,
      }),
    );
  }

  async #setVisibilityState(documentUri?: vscode.Uri) {
    const isCommentThreadActive = documentUri === this.#thread?.uri;
    const isExpanded =
      this.#thread?.collapsibleState === vscode.CommentThreadCollapsibleState.Expanded;
    this.#isOpen = isCommentThreadActive && isExpanded;

    await vscode.commands.executeCommand('setContext', 'gitlab:quickChatOpen', this.#isOpen);

    const editor = vscode.window.activeTextEditor;
    if (!editor) return;

    this.#updateKeybindingHint(editor);
  }

  dispose() {
    this.#disposables.forEach(disposable => disposable?.dispose());
    this.#gutterIconDecoration?.dispose();
  }
}
