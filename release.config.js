/* eslint-disable no-template-curly-in-string */
const isPrerelease = process.env.IS_PRERELEASE === 'true' || process.argv.includes('--prerelease');
const releaseType = isPrerelease ? 'pre-release' : 'release';
const headerPartial = `{{#if isPatch~}}  ##{{~else~}}  #{{~/if}} **${releaseType}**  {{#if @root.linkCompare~}}  [{{version}}](  {{~#if @root.repository~}}    {{~#if @root.host}}      {{~@root.host}}/    {{~/if}}    {{~#if @root.owner}}      {{~@root.owner}}/    {{~/if}}    {{~@root.repository}}  {{~else}}    {{~@root.repoUrl}}  {{~/if~}}  /compare/{{previousTag}}...{{currentTag}}){{~else}}  {{~version}}{{~/if}}{{~#if title}} "{{title}}"{{~/if}}{{~#if date}} ({{date}}){{/if}}`;

module.exports = {
  branches: 'main',
  plugins: [
    '@semantic-release/commit-analyzer',
    [
      '@semantic-release/release-notes-generator',
      {
        preset: 'angular',
        writerOpts: {
          headerPartial,
        },
      },
    ],
    '@semantic-release/changelog',
    [
      '@semantic-release/npm',
      {
        npmPublish: false,
      },
    ],
    [
      '@semantic-release/exec',
      {
        prepareCmd: './scripts/semantic-release-prepare.sh',
      },
    ],
    [
      '@semantic-release/git',
      {
        assets: ['package.json', 'package-lock.json', 'CHANGELOG.md'],
        message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}',
      },
    ],
    [
      '@semantic-release/gitlab',
      {
        assets: [
          {
            path: 'dist-desktop/*.vsix',
            label: 'gitlab-workflow-${nextRelease.version}.vsix',
            target: 'generic_package',
            type: 'package',
          },
        ],
      },
    ],
    [
      'semantic-release-slack-bot',
      {
        notifyOnSuccess: false,
        notifyOnFail: false,
        markdownReleaseNotes: true,
        slackWebhookEnVar: 'SLACK_WEBHOOK',
        branchesConfig: [
          {
            pattern: 'main',
            notifyOnSuccess: true,
            notifyOnFail: true,
          },
        ],
        onSuccessTemplate: {
          text: '🚀🚀🚀 *GitLab Workflow* extension has been released 🚀🚀🚀 \n\n$release_notes',
        },
      },
    ],
  ],
};
